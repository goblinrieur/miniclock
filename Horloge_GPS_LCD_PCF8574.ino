/*
	pris en exemple et à adapter au capteur GPS et à mon modèle d'arduino et à mon modèle d'écran I2C
	http://bidouilleur.ca/Bidouilleur_depot/fichiers_videos/EB_511_Cr%C3%A9ation_Horloge_Num%C3%A9rique_Exacte.zip
*/

#include <Wire.h>                       // La librairie I2C interfacer à l'affichage LCD via un backpack I2C PCF8574
#include <LiquidCrystal_PCF8574.h>
#define addr_LCD 0x27                   // L'adresse I2C du LCD A 
#include <SoftwareSerial.h>             // Utilisation d'un UART logiciel pour recevoir les données du GPS
#define pps 4                           // La broche d'entrée du signal PPS

LiquidCrystal_PCF8574 lcd(addr_LCD);    // Configure l'adresse I2C du LCD à 0x27
SoftwareSerial portOne(2, 3);           // Broches utilisées pour le port série soft (rx,tx)

const String    gprmc_preamble          = "$GPRMC";                                   // $GPRMC  Beginning of the NMEA string used to extract time and date
const char      days_in_months[]        = {31,28,31,30,31,30,31,31,30,31,30,31};      // Lists the number of days in each month. Used to calculate the day number.                                    
char            rx_char;                // Caractère reçu du port série soft
unsigned char   a;                      // utilisé comme compteur   
unsigned char   b;                      // utilisé comme compteur 
unsigned char   c;                      // utilisé comme compteur 
unsigned short  day_of_year;            // De 1 à 366 (le jour de l'année)
bool            leap_year;              // Utilisé pour marquer que c'est une année bissextile
bool            pps_low;                // Utilisé pour marquer que le signal PPS est au niveau bas, et prêt pour la transition bas-haut
bool            data_ready;             // Utilisé pour marquer que la phrase du GPS a été reçue et est prète à être analysée
unsigned char   hrs;                    // Valeur numérique de l'heure
unsigned char   mins;                   // Valeur numérique de la minute
unsigned char   secs;                   // Valeur numérique de la seconde
unsigned char   day_val;                // Valeur numérique du jour
unsigned char   month_val;              // Valeur numérique du mois
unsigned char   year_val;               // Valeur numérique de l'année
String          hrs_string;             // Valeur texte à afficher de l'heure
String          mins_string;            // Valeur texte à afficher de la minute
String          secs_string;            // Valeur texte à afficher de la seconde
String          day_string;             // Valeur texte à afficher du jour           
String          month_string;           // Valeur texte à afficher du mois
String          year_string;            // Valeur texte à afficher de l'année
String          day_of_year_string;     // Valeur texte à afficher du jour de l'année
String          gps_rx_string;          // Phrase GPS (chaîne de caractères) reçue
String          lcd_string;             // Une chaîne de caractères à afficher

// FONCTION D'INITIALISATION
void setup() 
{
  pinMode(pps, INPUT_PULLUP);           // Configurer la broche PPS comme entrée
  Serial.begin(9600);                   // Ouvrir le port Serie-USB
  portOne.begin(9600);                  // Ouvrir le port Serie logiciel provenant du GPS

  // Initialisation du port i2c du LCD
  Wire.begin();                         // Initialisation de la communication I2C avec le LCD

  // Définir et initialiser le LCD
  lcd.begin(16, 2);                     // Définir et initialiser le LCD comme ayant 16 colonnes et 2 lignes
  lcd.setBacklight(255);                // Allumer le rétro-éclairage
  lcd.home();                           // positionner le curseur à la première case
  lcd.clear();                          // Effacer l'écran

  // Affichage du logo d'allumage
  lcd.setCursor(0, 0);                  // positionner le curseur
  lcd.print("Electro-");                // Afficher le texte entre apostrophes
  lcd.setCursor(4, 1);                  // positionner le curseur
  lcd.print("Bidouilleur!");            // Afficher le texte entre apostrophes
  Serial.print("Electro-Bidouilleur!");
  delay(1500);
  lcd.clear();                          // Effacer l'écran
  lcd.setCursor(0, 0);                  // positionner le curseur
  lcd.print("H: --:--:-- UTC");         // Afficher le texte entre apostrophes
  lcd.setCursor(0, 1);                  // positionner le curseur
  lcd.print("D: --/--/-- ---");         // Afficher le texte entre apostrophes
}

// FONCTION DE BOUCLE PERPÉTUELLE
void loop()
{      
  while (portOne.available() > 0)                               // Boucler tant qu'il y a des caractères de déjà reçus du GPS
  {
    rx_char = portOne.read();                                   // Lire un caractère
    gps_rx_string = gps_rx_string + rx_char;                    // L'ajouter à la chaîne de caractères déjà reçues    
    Serial.print(rx_char);                                      // Afficher le caractère au port USB (Ordi)
    if (rx_char == '\n')                                        // Si le caractère est un saut de ligne, traiter la chaîne
    {                                                           // Le format attendu de la chaîne est:  $GPRMC,hhmmss.ss,A,llll.ll,a,yyyyy.yy,a,x.x,x.x,ddmmyy,x.x,a*hh
      if ((gps_rx_string.substring(0, 6) == gprmc_preamble)     // Comparer les 6 premiers caractères de la chaîne avec le preambule GPS désiré...
          && (gps_rx_string.substring(7) != ","))               // ...et vérifier que la chaîne n'est pas vide (GPS en initialisation)
      {                                                         // Oui, il y a correspondance, traitrer la chaîne

        // Extraction de l'heure
        day_of_year = 0;                                        // initialiser le compteur de jour de l'année
        hrs = gps_rx_string.substring(7,9).toInt();             // Extraire la valeur numérique de l'heure
        mins = gps_rx_string.substring(9,11).toInt();           // Extraire la valeur numérique de la minute
        secs = gps_rx_string.substring(11,13).toInt();          // Extraire la valeur numérique de la seconde
        if (++secs == 60)                                       // AJOUTER UNE SECONDE puisque la phrase GPS arrive après l'impulsion PPS. Ensuite traiter le dépassement des secondes?
        {                                                       // Oui
          secs = 0;                                             // Ré-initialiser les secondes
          if (++mins == 60)                                     // Ensuite traiter le dépassement des minutes?
          {                                                     // Oui
            mins = 0;                                           // Ré-initialiser les minutes
            if (++hrs == 24)                                    // Ensuite traiter le dépassement des heures?
            {                                                   // Oui
              hrs = 0;                                          // Ré-initialiser les heures
            }
          }
        }
        hrs_string = String(hrs);                               // Convertir la valeur numérique des heures en chaîne de caractères
        if (hrs < 10) hrs_string = "0" + hrs_string;            // Ajouter un zéro en avant si nécessaire
        mins_string = String(mins);                             // Convertir la valeur numérique des minutes en chaîne de caractères
        if (mins < 10) mins_string = "0" + mins_string;         // Ajouter un zéro en avant si nécessaire
        secs_string = String(secs);                             // Convertir la valeur numérique des secondes en chaîne de caractères
        if (secs < 10) secs_string = "0" + secs_string;         // Ajouter un zéro en avant si nécessaire

        //  Extraction de la date
        b = 0;                                                  // utilisé comme compteur de virgules
        c = 0;                                                  // utilisé comme compteur de caractères
        leap_year = false;                                      // Initialiser le marqueur d'année bissextile comme faux.
        do                                                      // Repérer le champ de la date dans la chaîne GPS reçue.
        { 
          if (gps_rx_string[c++] == ',') b++;                   // Incrémenter le compteur de virgule si une d'entre elle est détectée
        } 
        while (b < 9);                                          // Boucler si nécessaire car la date est localisée après la 9ème virgule
        day_val = gps_rx_string.substring(c,c+2).toInt();       // Extraire la chaîne de caractères représentant le jour
        month_val = gps_rx_string.substring(c+2,c+4).toInt();   // Extraire la chaîne de caractères représentant le mois
        year_val = gps_rx_string.substring(c+4,c+6).toInt();    // Extraire la chaîne de caractères représentant l'année
        if (((year_val%4 == 0) && (year_val%100 != 0)) || (year_val%400 == 0)) leap_year = true;  // Vérifier si l'année courante est bissextile?
        else leap_year = false;
        if ((secs == 0) && (mins == 0) && (hrs == 0))           // Traiter le changement de jour (date)?
        {                                                       // Oui
          day_val++;                                            // Incrémenter la valeur du jour
          if ((day_val > (days_in_months[month_val-1])) && !((leap_year) && (month_val == 2) && (day_val == 29))) // Traiter le changement de mois? (prenant en compte les années bissextiles)
          {                                                     // Oui
            month_val++;                                        // Incrémenter la valeur du mois
            day_val = 1;                                        // Réinitialiser le jour à 1
          }
          if (month_val > 12)                                   // Traiter le changement d'année?
          {                                                     // Oui
            year_val++;                                         // Incrémenter la valeur de l'année
            month_val = 1;                                      // Réinitialiser le mois à 1
            day_val = 1;                                        // Réinitialiser le jour à 1
          }
        }
        day_string = String(day_val);                           // Convertir la valeur numérique du jour en chaîne de caractères
        if (day_val < 10) day_string = "0" + day_string;        // Ajouter un zéro en avant si nécessaire
        month_string = String(month_val);                       // Convertir la valeur numérique du mois en chaîne de caractères
        if (month_val < 10) month_string = "0" + month_string;  // Ajouter un zéro en avant si nécessaire
        year_string = String(year_val);                         // Convertir la valeur numérique de l'année en chaîne de caractères
         
        // Calcul du jour de l'année
        day_of_year = 0;                                                                    // Initialiser le compteur du nombre de jours
        for (a = 0; a < (month_val - 1); a++)                                               // Calculer le nombre de jours dans tous les mois précédents (complétés)
        {
          day_of_year = day_of_year + days_in_months[a];                                    // Ajouter les jours du mois analysé
        }
        day_of_year = day_of_year + day_val;                                                // Ajouter les jours du mois courant
        if (leap_year && (a > 1)) day_of_year++;                                            // Si oui, ajouter une journée après le 29 Février
        if (!(hrs | mins | secs))                                                           // Vérifier si un nouveau jour survient
        {                                                                                   // Oui
          day_of_year++;                                                                    // Incrementer le jour de l'année quand l'heure est 00:00:00, sinon, cela arrivera seulement à 00:00:01
          if (day_of_year == (366 + leap_year))  // Traiter le dépassement du jour en fin d'année? 
          {                                                                                 // Oui
            day_of_year = 1;                                                                // Remettre le jour à 1.
          }
        }
        day_of_year_string = String(day_of_year);                                   // Convertir la valeur numérique du jour de l'année en chaîne de caractères
        if (day_of_year < 10) day_of_year_string = "00" + day_of_year_string;       // Ajouter deux zéros en avant si nécessaire
        else if (day_of_year < 100) day_of_year_string = "0" + day_of_year_string;  // Sinon, ajouter un zéro en avant si nécessaire

        // Vérification finale avant affichage. Élimine un affichage erronné.
        if ((day_val >= 1 && day_val <= 31)                                         // Est-ce que toutes les valeurs à afficher sont dans une plage qui a un sens?
           && (month_val >= 1 && month_val <= 12)
           && (year_val >= 22 && year_val <= 99)
           && (hrs >= 0 && hrs <= 23)
           && (mins >= 0 && mins <= 59)
           && (secs >= 0 && secs <= 59)
           && (day_of_year >= 1 && day_of_year <= 366))
        {                                                                           // Oui
          data_ready = true;                                                        // Toutes les valeurs ont du sens. Signaler que les données extraites sont valides et prêtes à être affichées.
        }
      }      
      gps_rx_string = "";                                                           // Après réception du caractère de fin de phrase et traitement de la chaîne, effacer la chaîne de caractères reçus
    }
  }
  if (!(digitalRead(pps)))                            // Niveau bas sur le signal PPS ?
  {                                                   // Oui
    pps_low = true;                                   // Activer le marqueur de niveau bas de PPS
  }
  if ((pps_low) && (digitalRead(pps)))                // Le PPS était précédemment bas et qu'il est maintenant haut?
  {                                                   // Oui
    if (data_ready)                                   // Données valides et prètes à être affichées?
    {                                                 // Oui
      lcd.setCursor(0, 0);                            // Positionner le curseur en début de 1ère ligne
      lcd_string = "H: " + hrs_string + ":" + mins_string + ":" + secs_string + " UTC";  // Formatter la chaîne de caractères de la 1ère ligne
      lcd.print(lcd_string);                          // Afficher la chaîne formattée
      lcd.setCursor(0, 1);                            // Positionner le curseur en début de 2è ligne
      lcd_string = "D: " + day_string + "/" + month_string + "/" + year_string + " " + day_of_year_string;  // Formatter la chaîne de caractères de la 2è ligne
      lcd.print(lcd_string);                          // Afficher la chaîne formattée
      data_ready = false;
    }
    else                                              // Données à afficher non valides. Remplir les champs avec des tirets.
    {
      lcd.setCursor(0, 0);                            // Positionner le curseur en début de 1ère ligne
      lcd.print("H: --:--:-- UTC");                   // Afficher le texte entre apostrophes
      lcd.setCursor(0, 1);                            // Positionner le curseur en début de 2è ligne
      lcd.print("D: --/--/-- ---");                   // Afficher le texte entre apostrophes
    }
    pps_low = false;                                  // Texte affiché, le PPS est donc au niveau haut. Desactiver le marqueur de niveau bas de PPS
  }
}
 
